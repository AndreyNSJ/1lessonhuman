package com.company;

public class Main{

    public static void main(String[] args) {
        Human vasa1 = new Human("Vasa", "Pupkin", "Olegovic");
        Human vasa2 = new Human("Vasa", "Pupkin");
        vasa1.getFullName();
        vasa2.getFullName();
        vasa1.getShortName();
        vasa2.getShortName();
    }
}
class Human {
    private String name;
    private String surname;
    private String patronymic = "emptyPat";

    public Human(String name, String surname, String patronymic){

        this.name=name;
        this.surname=surname;
        this.patronymic=patronymic;
    }

    public Human(String name, String surname){
        this.name=name;
        this.surname=surname;
    }

    public void getFullName(){
        if (patronymic == "emptyPat") {
            System.out.println(name + " " + surname);
        } else{
            System.out.println(name + " " + surname + " " + patronymic);
        }
    }

    public void getShortName(){
        surname = surname.charAt(0) + ".";
        if (patronymic != "emptyPat")
            patronymic = patronymic.charAt(0) + ".";
        getFullName();
    }
}


